﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SeminarioTdd.Tests
{
    public class ValidadorDeNumerosTeste
    {
        // http://xunit.github.io/
        // Os fatos são testes que são sempre verdadeiros. Eles testam condições invariáveis.
        // Teorias são testes que são verdade apenas para um determinado conjunto de dados.

        [Fact]
        private void DeveValidarNumerosPares()
        {
            var numeroPar = IsNumeroPar(2);

            //Assert.Equal(numeroPar, true);
            Assert.True(numeroPar);
        }

        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(6)]
        public void TeoriaNumerosParesValidos(int value)
        {
            Assert.True(IsNumeroPar(value));
        }

        [Theory]
        [InlineData(1)]
        [InlineData(7)]
        [InlineData(99)]
        [InlineData(3)]
        [InlineData(27)]
        [InlineData(43)]
        public void TeoriaNumerosImparesInvalidos(int value)
        {
            Assert.False(IsNumeroPar(value));
        }

        [Theory]
        [InlineData(7)]
        [InlineData(11)]
        [InlineData(13)]
        public void TeoriaNumerosPrimosValidos(int value)
        {
            Assert.True(IsNumeroPrimo(value));
        }

        [Theory]
        [InlineData(1)]
        [InlineData(8)]
        [InlineData(9)]
        public void TeoriaNumerosPrimosInvalidos(int value)
        {
            Assert.False(IsNumeroPrimo(value));
        }

        private bool IsNumeroPar(int numero)
        {
            return numero % 2 == 0;
        }

        private bool IsNumeroPrimo(int numero)
        {
            if (numero == 1)
                return false;

            for (int i = 2; i * i <= numero; i++)
            {
                if (numero % i == 0)
                    return false;
            }

            return true;
        }
    }
}
